/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2021-03-28 21:47:04
 * @modify date 2021-03-28 21:47:04
 * @desc [description]
 */

import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { typeDefs } from './lib/graphql/typeDefs';
import { resolvers } from './lib/graphql/resolvers';
import databaseConnection from './db';

const app = express();

databaseConnection.
then(success => {
    // definiendo la configuracion para el servidor Apollo
    const server = new ApolloServer({ typeDefs, resolvers });
    // aplicando los Middlewares para nuestro servidor Express (Apollo hará todo el trabajo de poner bodyparser, etc)
    server.applyMiddleware({ app });
    //subiendo el servidor 
    app.listen({ port: 3001 }, () => {
        console.log(`🚀 Server ready at http://localhost:3001${server.graphqlPath}`);
    });
    console.log(success)
})
.catch(error => {
    console.log(`Error al subir el API --> ${error}`)
});
