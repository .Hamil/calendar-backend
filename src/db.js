import mongoose from 'mongoose';
import config from './config';

//Lo exportamos como una promesa para luego ejecutar nuestro servidor en el then
export default new Promise((resolve, reject) => {
    try {
        mongoose.connect(config.mongoUrl, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
        resolve(`Conexión exitosa a la base de datos`);
    } catch (error) {
        reject(error)
    }
});
