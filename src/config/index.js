/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2021-03-28 21:41:27
 * @modify date 2021-03-28 21:41:27
 * @desc [description]
 */

export default {
    "port": 3001,
    "bodyLimit": "100kb",
    "mongoUrl": "mongodb://localhost:27017/api-calendar"
}