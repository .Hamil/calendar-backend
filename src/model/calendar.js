import mongoose from 'mongoose';
const Schema = mongoose.Schema;

//declarando la estructura de nuestros documentos
let Calendar = new Schema({
    title: String,
    allDay: Boolean,
    start: Date,
    end: Date,
    desc: String
});

module.exports = mongoose.model('Calendar', Calendar);
