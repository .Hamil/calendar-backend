import { gql } from 'apollo-server-express';

//declarando la estructura de nuestros metodos en GraphQL
export const typeDefs = gql`
scalar Date

type Query {
    events: [Event!]!
}
type Event {
    id: ID!
    title: String!
    desc: String
    allDay: Boolean!
    start: Date
    end: Date
}

type Mutation {
    createEvent(title: String!, desc: String!, allDay: Boolean, start: Date, end: Date): Event!
}
`;