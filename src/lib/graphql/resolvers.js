import Calendar from '../../model/calendar';
import { Date } from './scalars';

//poniendo nuestra logica en GraphQL
export const resolvers = {
    Query: {
        events: () => Calendar.find()
    },
    Mutation: {
        createEvent: async (_, { title, desc, allDay, start, end }) => {
            const event = new Calendar({ title, desc, allDay, start, end });
            await event.save();
            return event;
        }
    },
    Date: Date
}