import { GraphQLScalarType, Kind } from 'graphql';

//aqui pondremos todos los Scalars (tipos de datos que GraphQL no maneja, pero que nosotros podemos customizar)
export const Date = () => new GraphQLScalarType({
    name: "Date",
    description: "Un tipo de dato exclusivo para manejar fecha",
    serialize(value) {
      return value; // Convirtiendo la informacion que devolvemos
    },
    parseValue(value) {
      return new Date(value); // Convirtiendo la informacion que tomamos del cliente
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.STRING) {
        return new Date(value);
      }
      return null;
    },
  });